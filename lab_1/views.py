from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Aryo Tinulardhi' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1999, 5, 6) #TODO Implement this, format (Year, Month, Date)
npm = 1706039515 # TODO Implement this
# Create your views here.
def index(request):
    response = {'name': mhs_name,
                'age' : calculate_age(birth_date.year),
                'npm' : npm,
                'univ' : "Universitas Indonesia",
                'hobi' : 'Ngoding',
                'desc' : 'Just a human who can give some good impact to your life. :)',
                'friend1' : "Jeremia Delviero",
                'npm_f1' : 1706039875,
                'univ_f1' : "Universitas Indonesia",
                'hobi_f1' : 'playing games and doing homework as soon as possible.',
                'desc_f1' : 'I think I am a nice person though have negligible weaknesses, have a good amount of likeable good qualities too. I am sincere and responsible. I am not a very intelligent student but sure I am dynamic as I am capable of managing and handling serious and difficult situations easily and finish all tasks well. I like to stay simple and uncomplicated and live joyously enjoying every moment of my life.',

                'friend2' : "George Matthew Limongan",
                'npm_f2' : 1706043891,
                'univ_f2' : "Universitas Indonesia",
                'hobi_f2' : 'sleep',
                'desc_f2' : 'Right now busy studying on my third semester',
                }
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
